var containsArr;
var carouselArray = [];
var SearcResult=[];
var pageFrom="";

function recentwatched() {
    
    var from = "'recently'";
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
     myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;

                $.ajax({
                    type: "POST",
                    url: ServerUrl + "fetchRecentlyWatched",
                    data: JSON.stringify({
                        "fetchRecentlyWatched": {
                            "id": UserID,
                            "userId": UserID,
                            "userType": "web"
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        if (data.recentlyWatched && data.recentlyWatched.length > 0) {
                            var HTMLContent = "";
                            var CourosalID = "recentlyWatched";
                            var HTMLContent = ' <div class="blk1" ><h3 >Recently Watched</h3><div id="' + CourosalID + '" class="carousel slide get_stt" ><div class="carousel-inner" id="items_' + CourosalID + '" > ';
                            var i;
                            for (i = 0; i < data.recentlyWatched.length; i++) {
                                {
                                    /////////////////////////////////////
                                    var RName = "'" + data.recentlyWatched[i].data.metadata.carousel_id + "'";
                                    var remainder = i % 2;
                                    if (remainder == 0 && i > 0) {
                                        var HTMLContent = HTMLContent + '<div class="item" ><div class="span3"><a href="#x" onClick="showDetails(' + data.recentlyWatched[i].data.id + ',' + RName + ','+from+')" class="thumbnail"><img src="' + data.recentlyWatched[i].data.metadata.movie_art + '" alt="' + data.recentlyWatched[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div>';
                                        if (data.recentlyWatched.length == i + 1) {
                                            var HTMLContent = HTMLContent + '</div>';
                                        }

                                    } else {
                                        if (i == 0 || i == 1) {
                                            if (i == 0) {

                                                var HTMLContent = HTMLContent + '<div class="item active"  ><div class="span3"><a href="#x" onClick="showDetails(' + data.recentlyWatched[i].data.id + ',' + RName + ','+from+')" class="thumbnail"><img src="' + data.recentlyWatched[i].data.metadata.movie_art + '" alt="' + data.recentlyWatched[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div>'

                                            } else {
                                                var HTMLContent = HTMLContent + '<div class="span3"><a href="#x" onClick="showDetails(' + data.recentlyWatched[i].data.id + ',' + RName + ','+from+')" class="thumbnail"><img src="' + data.recentlyWatched[i].data.metadata.movie_art + '" alt="' + data.recentlyWatched[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div></div> ';
                                            }
                                        } else {

                                            var HTMLContent = HTMLContent + '<div class="span3"><a href="#x" onClick="showDetails(' + data.recentlyWatched[i].data.id + ',' + RName + ','+from+')" class="thumbnail"><img src="' + data.recentlyWatched[i].data.metadata.movie_art + '" alt="' + data.recentlyWatched[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div></div>';
                                        }
                                    }
                                    ////////////////////////////////////////////////

                                    if (data.recentlyWatched.length == i + 1) {
                                        var HTMLContent = HTMLContent + '</div></div></div>';
                                        $("#cat_" + CourosalID).html('');
                                        $("#cat_" + CourosalID).hide();
                                        $("#cat_" + CourosalID).html(HTMLContent);
                                        $("#cat_" + CourosalID).fadeIn(700);
                                        $('#' + CourosalID).carousel({
                                            interval: 0
                                        });

                                        $("#" + CourosalID).swiperight(function() {

                                            $(this).carousel('prev');
                                        });
                                        $("#" + CourosalID).swipeleft(function() {

                                            $(this).carousel('next');
                                        });

                                    }
                                }

                            }

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        showAlert('Server error! Please try later');
                    },
                    complete: function() {


                    }
                });

            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });

}

setTimeout(function(){recentwatched();},2000);

function getMyList() {
	var from = "'mylist'";
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var NavItems = [];
                var UserID = results.rows.item(i).key3;
                var CourosalID = "myList";
                $("#cat_" + CourosalID).html('');

                $.ajax({
                    type: "POST",
                    url: ServerUrl + "fetchMyList",
                    data: JSON.stringify({
                        "fetchMyList": {
                            "id": UserID,
                            "userId": UserID,
                            "userType": "web"
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        if (data.status) {
                            showAlert('My List Error:' + data.status);
                        } else {
                            if (data.myList && data.myList.length > 0) {
                                var HTMLContent = "";

                                var HTMLContent = ' <div class="blk1"><h3 >My List</h3><div id="' + CourosalID + '" class="carousel slide get_stt" ><div class="carousel-inner" id="items_' + CourosalID + '" > ';
                                var i;
                                for (i = 0; i < data.myList.length; i++) {
                                    {
                                        var MName = "'" + data.myList[i].data.metadata.carousel_id + "'";
                                        /////////////////////////////////////
                                        var remainder = i % 2;
                                        if (remainder == 0 && i > 0) {
                                            var HTMLContent = HTMLContent + '<div class="item" ><div class="span3"><a href="#x" onClick="showDetails(' + data.myList[i].data.id + ',' + MName + ','+from+')" class="thumbnail"><img src="' + data.myList[i].data.metadata.movie_art + '" alt="' + data.myList[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div>';
                                            if (data.myList.length == i + 1) {
                                                var HTMLContent = HTMLContent + '</div>';
                                            }

                                        } else {
                                            if (i == 0 || i == 1) {
                                                if (i == 0) {

                                                    var HTMLContent = HTMLContent + '<div class="item active"  ><div class="span3"><a href="#x" onClick="showDetails(' + data.myList[i].data.id + ',' + MName + ','+from+')" class="thumbnail"><img src="' + data.myList[i].data.metadata.movie_art + '" alt="' + data.myList[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div>'

                                                } else {
                                                    var HTMLContent = HTMLContent + '<div class="span3"><a href="#x" onClick="showDetails(' + data.myList[i].data.id + ',' + MName + ','+from+')" class="thumbnail"><img src="' + data.myList[i].data.metadata.movie_art + '" alt="' + data.myList[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div></div> ';
                                                }
                                            } else {

                                                var HTMLContent = HTMLContent + '<div class="span3"><a href="#x" onClick="showDetails(' + data.myList[i].data.id + ',' + MName + ','+from+')" class="thumbnail"><img src="' + data.myList[i].data.metadata.movie_art + '" alt="' + data.myList[i].data.metadata.movie_art + '" style="max-width:100%;" /></a></div></div>';
                                            }
                                        }
                                        ////////////////////////////////////////////////

                                        if (data.myList.length == i + 1) {
                                            var HTMLContent = HTMLContent + '</div></div></div>';

                                            $("#cat_" + CourosalID).hide();
                                            $("#cat_" + CourosalID).html(HTMLContent);
                                            $("#cat_" + CourosalID).fadeIn(700);
                                            $('#' + CourosalID).carousel({
                                                interval: 0
                                            });

                                            $("#" + CourosalID).swiperight(function() {

                                                $(this).carousel('prev');
                                            });
                                            $("#" + CourosalID).swipeleft(function() {

                                                $(this).carousel('next');
                                            });

                                        }
                                    }

                                }

                            }
                        }



                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        showAlert('Server error! Please try later');
                    },
                    complete: function() {;
                    }
                });


            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });

}

function getCarousels() {

    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var NavItems = [];
                var UserID = results.rows.item(i).key3;
                $.ajax({
                    url: ServerUrl + "getCarousels?device=IosApp",
                    type: "POST",
                    data: JSON.stringify({
                        "getCarousels": {
                            "id": UserID,
                            "userType": "web"
                        }
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function(data) {
                        if (data.status) {
                            showAlert(data.status);
                            Logout();
                        } else {
                            $("#carousalList").html('');
                            $("#navList").html('<li><a href=javascript:selectHome()>Home</a></li>');
                            if (data && data.length > 0) {
                                var i;
                                var NavItems = [];
                                for (i = 0; i < data.length; i++) {
                                    $("#navList").append('<li><a href=javascript:selectedItems("' + data[i].carousel_name.replace(/ /g, '') + '")>' + data[i].carousel_name + '</a></li>');
                                    $("#carousalList").html($("#carousalList").html() + '<div id="cat_' + data[i].carousel_name.replace(/ /g, '') + '"><div class="preloaderCont"><img class="preloaderLogo" src="img/logo_preloader.png"/><div class="preloader3"></div></div></div>');
                                    // setTimeout(getCarouselItem(data[i].carousel_name) ,10000)
                                    //NavItems.push(data[i].carousel_name);
                                    if (data.length == i + 1) {
                                        // alert(NavItems); alert(NavItems.length);
                                        // var itemtoRemove = data[i].carousel_name;
                                        // NavItems.splice($.inArray(itemtoRemove, NavItems), 1);    
                                        //alert(NavItems); alert(NavItems.length);
                                        // getCarouselItem(NavItems);
                                    }
                                    getCarouselItem(data[i].carousel_name);
                                }

                            }
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                    },
                    complete: function() {
                        //alert(NavItems.length);
                        getMyList();
                    }
                });
            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });


}


function playVideo(id, videoUrl) {
    AddToRecent(id);
    var videoUrl = videoUrl.replace("https:", "http:");
    $.ajax({
        url: ServerUrl + "getAssestData",
        type: "POST",
        data: '{"getAssestData": {"videoId": "' + id + '", "userId": "' + $("#UID").val() + '","returnType":"tiny"  }}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        traditional: true,
        success: function(data) {
            if (data[0].subscription_status == 'active') {
                // Just play a video 
                var options = {
                    seekTime: parseInt(data[0].watchedVideo),
                    userId:$("#UID").val(),
                    videoId:id,
                    serverURL:ServerUrl+'updateSeekTime',
                    successCallback: function(obj) {      
                    },
                    errorCallback: function(errMsg) {
                        console.log("Error! " + errMsg);
                    }
                };
                window.plugins.streamingMedia.playVideo(videoUrl, options);


            } else {
                loadLocalPage('myAccount.html');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Server error! Please try later");
        }
    });



}



function updateSeekTime(id, hms) {

    var a = hms.split(':'); // split it at the colons
    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var Vseconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
    if (Vseconds > 0) {
        $.ajax({
            type: "POST",
            url: ServerUrl + "updateSeekTime",
            // The key needs to match your method's input parameter (case-sensitive).
            data: '{"updateSeekTime": {"userId": "' + $("#UID").val() + '", "videoId": "' + id + '", "seekTime": "' + Vseconds + '"}}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                JSON.stringify(data);
            },
            failure: function(errMsg) {
                showAlert(errMsg);
            }
        });
    }
}

function AddToRecent(id) {
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;
                $.ajax({
                    url: ServerUrl + "createRecentlyWatched",
                    type: "POST",
                    data: '{"createRecentlyWatched": {"videoId": "' + id + '", "userId": "' + UserID + '"}}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    traditional: true,
                    success: function(data) {
                        recentwatched();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        showAlert("Server error! Please try later");
                    }
                });
            } else {
                showAlert('No user found.');
                Logout();
            }


        }, null);
    });
}

function selectedItems(id) {
    // $("#contentWrapList").hide();
    $("#selectedCat").html('<div class="sc" >' + $("#items_h_" + id).html() + '</div><div class="shw_blk"><div class="container-fluid"><div class="row nomarg">' + $("#items_" + id).html() + '</div></div></div>');
    $("#contentWrapList").hide();
    setTimeout(function() {
        $("#contentWrapList").fadeIn(700);
    }, 200);
    $("#carousalWrap").hide();
    $("#detailedWrap").hide();
    $(".header_com").show();
    /* $("#contentWrapList").animate({
        'marginTop': '-495px'
    });*/
    // $("#selectedCat").fadeIn(700);
    toggleNav();
    $("#carousalWrap").addClass("animatedSlow fadeInLeft");
}

function selectHome() {
    $("#contentWrapList").hide();
    $("#selectedCat").html('');
    $("#carousalWrap").fadeIn(700);
    $("#detailedWrap").hide();
    $(".header_com").show();
    toggleNav();
}

function showDetails(id, name,from) {
	 pageFrom=from; //assiging this value to use at back/close button 

    $("#loading").show();
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;
                $("#UID").val(UserID);
                $.ajax({
                    url: ServerUrl + "getAssestData",
                    type: "POST",
                    data: '{"getAssestData": {"videoId": "' + id + '", "userId": "' + UserID + '","returnType":"tiny"}}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    traditional: true,
                    success: function(data) {
                    	 if(pageFrom=='search')
						 {
						 	$("#popupPage").hide();
						 }
                        $("#seasons").html("");
                        $("#Season").hide();
                        $("#loading").hide();
                        $("#detailedWrap").hide();
                        $("#detailedThumb").html('');
                        $("#video_desc").html('');
                        $("#vy").html('');
                        $("#vc").html('');
                        $("#episodes").html('');
                        $("#manageMyList").html('');
                        $("#detailedWrap").fadeIn(700);
                        $("#carousalWrap").addClass("animatedSlow fadeInLeft");
                        $(".header_com").hide();
                        if (data[0].url_m3u8) {
                            var videoURL = data[0].url_m3u8;
                        } else {
                            var videoURL = data[0].url;
                        }
                        $("#detailedThumb").html('<img src="' + data[0].thumb + '" width="100%"> <a href="javascript:BackHandler()" class="closebtn"></a> <a href=javascript:playVideo(' + id + ',"' + videoURL + '") class="playbtn"></a> <div class="bnr_grn"><div class="blackscreen2"></div><h2 class="shwtitle">' + data[0].name + '</h2> </div>');
                        $("#video_desc").html(data[0].description);
                        $("#ad").html(durationOfAsset(data[0].file_duration));
                        if (data[0].release_date) {
                            $("#vy").html(data[0].release_date.split("-")[0]);
                        }
                        if (data[0].cast) {
                            $("#vc").html('<div>Cast : ' + data[0].cast + '</div><div>Creator  : ' + data[0].director + '</div>');
                        }
                        if (data[0].myList == true) {
                            $("#manageMyList").html('<a href="javascript:manageMylist(' + id + ')" class="addbtn" id="manageMyList">- My list</a>');
                        } else {
                            $("#manageMyList").html('<a href="javascript:manageMylist(' + id + ')" class="addbtn" id="manageMyList">+ My list</a>');
                        }

                        containsArr = data[0].contains;
                        var count = _.size(containsArr);
                        if (count > 0) {
                            var ss = "";
                            if (count > 1) {
                                ss = 's';
                            }
                            $("#seasons").html(count + " Season" + ss);
                            $("#ss_blk").html('Season 1');
                            $("#Season").html('');

                            if (count > 0) {
                                _.forEach(containsArr, function(value, key) {
                                    $("#Season").append('<li><a href="javascript:showEpisodes(' + key + ')">Season ' + key + '</a></li>');
                                });
                                showEpisodes(1);
                            }

                        }


                        if (count > 0) {
                            $("#episode_li").show();
                            $("#episode_li").addClass("active");
                            $("#home").addClass("active");
                            $("#like_li").removeClass("active");
                            $("#profile").removeClass("active");


                        } else {
                            $("#episode_li").removeClass("active");
                            $("#home").removeClass("active");
                            $("#like_li").addClass("active");
                            $("#profile").addClass("active");
                            $("#episode_li").hide();

                        }

                        if (count > 1) {
                            $("#ss_blk").removeClass('slt_n');
                        } else {
                            $("#ss_blk").addClass('slt_n');
                        }
                        getMoreLike(id, UserID, name,pageFrom);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
                        showAlert("Server error! Please try later");
                    }
                });

            } else {
                showAlert('No user found.');
                Logout();
            }



        }, null);
    });

}

$(document).ready(function() {
    $("#ss_blk").click(function() {
        $("#Season").fadeIn();
    });

    $('body').click(function(e) {
        if ($(e.target).attr('class') != 'slt') {
            $("#Season").fadeOut();
        }

    });
});


function showEpisodes(id) {
    $("#ss_blk").html('Season ' + id);
    $("#episodes").empty();

    $.each(containsArr[id], function(key, value) {

        $("#episodes").append('<p><div class="episod_title" >' + value.name + '</div></p>');
        if (value.url_m3u8) {
            var E_videoURL = value.url_m3u8;
        } else {
            var E_videoURL = value.url;
        }

        $("#episodes").append('<div><div class="pr_vd"><img src="' + value.thumb + '" width="100%"> <a href=javascript:playVideo(' + value.id + ',"' + E_videoURL + '") class="playbtn_tab"></a> </div></div>')
        $("#episodes").append('<p><div class="durtn">' + durationOfAsset(value.file_duration) + '</div></p>')
        $("#episodes").append('<p><div class="episode_desc" ><span>' + value.description + '</span></div></p>');

    });

}

function manageMylist(id) {
    $("#manageMyList").html('<a href="#" class="addbtn"  "><div class="preloaderWrap"><div class="zoomcir"></div><div class="zoomcir2"></div><div class="zoomcir3"></div></div></a>');
   var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;
                /*  console.log(id);
                 console.log("UserID");
                 console.log(UserID);*/
                $.ajax({
                    url: ServerUrl + "manageMyList",
                    type: "POST",
                    data: '{"manageMyList": {"videoId": "' + id + '", "userId": "' + UserID + '"}}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    traditional: true,
                    success: function(data) {
                        /* alert(JSON.stringify(data));
                         alert(data.myList);
                         alert(data.myList.ok);*/
                        if (data.myList.ok == '1') {
                            $("#manageMyList").html('<a href="javascript:manageMylist(' + id + ')" class="addbtn"  >+ My list</a>');
                        } else {
                            $("#manageMyList").html('<a href="javascript:manageMylist(' + id + ')" class="addbtn"  >- My list</a>');
                        }

                        getMyList();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        showAlert("Server error! Please try later");
                    }
                });
            } else {
                showAlert('No user found.');
                Logout();
            }



        }, null);
    });
}

function getMoreLike(id, UserID, name,pf) {
    var from = "'" + pf + "'";
    var carouselName = "'" + name + "'";
    $("#moreLike").html('');
    $("#more_like_loading").show();
    $.ajax({
        url: ServerUrl + "getMoreLikeThis",
        type: "POST",
        data: JSON.stringify({
            "getMoreLikeThis": {
                "id": UserID,
                "userType": "web",
                "name": name,
                "genre": "",
                "returnType": "tiny",
                "assestId": id
            }
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        traditional: true,
        success: function(data) {
            $("#more_like_loading").hide();
            $("#moreLike").hide();
            //alert(JSON.stringify(data));
            $.each(data.moreLikeThis, function(key, value) {
                //$("#moreLike").append('<a href="#" class="gm"><img src="'+value.thumb+'"></a>');
                $("#moreLike").append('<a href="#x" onclick="showDetails(' + value.id + ',' + carouselName + ','+from+')"><div style="background-image:url(' + value.thumb + '); background-repeat:no-repeat; background-position:50% 50%; height:16vh;width: 48%; background-size:120%; float:left; margin:1%;"></div></a>');
                // TODO fade effect..
            });
            $("#moreLike").fadeIn();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $("#more_like_loading").hide();
            showAlert("Server error! Please try later");
        }
    });
}

function durationOfAsset(duration) {
    var ms = parseInt(duration);
    var seconds = (ms / 1000);
    var minutes = parseInt(seconds / 60, 10);
    seconds = seconds % 60;
    var hours = parseInt(minutes / 60, 10);
    minutes = minutes % 60;
    var assetDuration = (hours + "h " + minutes + "m");
    return assetDuration;
}

function getCarouselItem(id) {
    {
    	var from = "'home'";
        carouselArray=[]; // empty arry before updating data
        var carouselName = "'" + id + "'";
        var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
        myDB.transaction(function(transaction) {
            transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
                var len = results.rows.length,
                    i = 0;
                if (len > 0) {
                    var UserID = results.rows.item(i).key3;
                    $.ajax({
                        url: ServerUrl + "getCarouselData",
                        type: "POST",
                        data: JSON.stringify({
                            "getCarouselData": {
                                "name": id,
                                "id": UserID,
                                "userType": "web"
                            }
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        traditional: true,
                        success: function(imgData) {

                            if (imgData[id] && imgData[id].length > 0) {




                                // Preaparing html
                                var HTMLContent = "";
                                var CourosalID = id.replace(/ /g, '');
                                var HTMLContent = ' <div class="blk1" ><div id="items_h_' + CourosalID + '"  ><h3>' + id + '</h3></div><div id="' + CourosalID + '" class="carousel slide get_stt" ><div class="carousel-inner" id="items_' + CourosalID + '" > ';
                                var i;

                                for (i = 0; i < imgData[id].length; i++) {
                                    // preparing html

                                    /////////////////////////////////////
                                    var remainder = i % 2;
                                    if (remainder == 0 && i > 0) {


                                        var HTMLContent = HTMLContent + '<div class="item" ><div class="span3"><a href="#x" onClick="showDetails(' + imgData[id][i].id + ',' + carouselName + ','+from+')" class="thumbnail"><img src="' + imgData[id][i].metadata.movie_art + '" alt="' + imgData[id][i].metadata.movie_art + '" style="max-width:100%;" /></a></div>';
                                        if (imgData[id].length == i + 1) {
                                            var HTMLContent = HTMLContent + '</div>';
                                        }

                                    } else {
                                        if (i == 0 || i == 1) {
                                            if (i == 0) {

                                                var HTMLContent = HTMLContent + '<div class="item active"  ><div class="span3"><a href="#x" onClick="showDetails(' + imgData[id][i].id + ',' + carouselName + ','+from+')" class="thumbnail"><img src="' + imgData[id][i].metadata.movie_art + '" alt="' + imgData[id][i].metadata.movie_art + '" style="max-width:100%;" /></a></div>'

                                            } else {
                                                var HTMLContent = HTMLContent + '<div class="span3"><a href="#x" onClick="showDetails(' + imgData[id][i].id + ',' + carouselName + ','+from+')" class="thumbnail"><img src="' + imgData[id][i].metadata.movie_art + '" alt="' + imgData[id][i].metadata.movie_art + '" style="max-width:100%;" /></a></div></div> ';
                                            }
                                        } else {

                                            var HTMLContent = HTMLContent + '<div class="span3"><a href="#x" onClick="showDetails(' + imgData[id][i].id + ',' + carouselName + ','+from+')" class="thumbnail"><img src="' + imgData[id][i].metadata.movie_art + '" alt="' + imgData[id][i].metadata.movie_art + '" style="max-width:100%;" /></a></div></div><div style="clear:both !important;"></div>';

                                        }
                                    }
                                    //////////////////////////////////////////////// 
                                    if (imgData[id].length == i + 1) {
                                        var HTMLContent = HTMLContent + '</div></div></div>';
                                        $("#cat_" + CourosalID).html('');
                                        $("#cat_" + CourosalID).hide();
                                        $("#cat_" + CourosalID).html(HTMLContent);
                                        $("#cat_" + CourosalID).fadeIn(700);

                                        $('#' + CourosalID).carousel({
                                            interval: 0
                                        });

                                        $("#" + CourosalID).swiperight(function() {
                                            $(this).carousel('prev');
                                        });
                                        $("#" + CourosalID).swipeleft(function() {

                                            $(this).carousel('next');
                                        });
                                    }
                                    // pushing to array for search
                                    carouselArray.push({
                                        "assetId": imgData[id][i].id,
                                        "assetName": imgData[id][i].name,
                                        "carouselName": carouselName,
                                        "image": imgData[id][i].metadata.movie_art,
                                    });
                                }

                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            showAlert("Server error! Please try later" + errorThrown);
                        },
                        complete: function() {

                        }
                    });
                } else {
                    showAlert('No user found.');
                    Logout();
                }
            }, null);
        });
    }
}
// Search

function SearchResult() {
	var from = "'search'";
	SearcResult=[]; // empty arry before updating data
    $("#searchContent").empty();
    Resultarr = $.grep(carouselArray, function(n, i) {
        var filter = myTrim($("#search").val());
 		var assetName=myTrim(n.assetName);
        	if(filter=='') 
       	{
 			$("#searchContent").html('<p class="ps">Search for Movies, TV Shows, Music & More</p>');
       	}
        else if (assetName.search(new RegExp(filter, "i")) > -1) {

                $("#searchContent").append('<div class="scrh_itm"><a href="#x" onclick="showDetails(' + n.assetId + ',' + n.carouselName + ','+from+')"  ><img src="' + n.image + '" style="max-width:100%;"></a></div>');
          }
          else
          {
          	SearcResult.push(n.carouselName)
          }
           
    });
   
    setTimeout(function() { 
    	if(carouselArray.length==SearcResult.length)
    	{
    		$("#searchContent").html('<p class="ps">No result found.</p>');
    	}
      }, 500)
}

// Logout 
function Logout() {
    $("#loading").show();
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(tx) {
        var qry = "DELETE FROM damedashSettings";
        tx.executeSql(qry, function() {});
    }, function errorCB(err) {
        $("#loading").hide();
        showAlert("Error processing SQL: " + err.code);
    }, function() {
        window.location.href = 'index.html';
    });

}

function showhide(id) {
    divid = "divid" + id;
    for (i = 1; i < 5; i++) {
        var text = ""
        text += "divid" + i;
        document.getElementById(text).style.display = "none";
    }
    if (id == 4) {

        $("#divid4").html('<h3 class="borderbottom">Payment</h3><a class="weightbold" onClick="showhide(3);">Back</a><div id="paymentMsg" align="center" class="colorred"></div><form id="checkout" method="post" action="/checkout"><div id="payment-form"></div> <input type="submit" value="Pay $' + $("#price").val() + '"></form>');

        /*             braintree.setup(clientToken, "dropin", {
                      container: "payment-form"
                    });*/

        braintree.setup(clientToken, "dropin", {
            container: "payment-form",
            onReady: function() {},
            onError: function(err) {
                console.error(err);
            },
            onPaymentMethodReceived: function(payload) {
                console.log(payload);
                var params = {
                    "checkout": {
                        planId: $("#planId").val(),
                        payment_method_nonce: payload.nonce,
                        user_id: $("#UID").val(),
                        customer_id: $("#customeID").val(),
                        subscription_end: $("#subscription_end").val()
                    }
                };
                var data = JSON.stringify(params);

                $.ajax({
                    url: ServerUrl + "checkout",
                    type: "POST",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    traditional: true,
                    success: function(data) {
                        console.log(data);

                        if (data.status == 'success') {
                            loadLocalPage('myAccount.html')
                        } else {
                            showhide(4);
                            $("#paymentMsg").html(data.error);
                            setTimeout(function() {
                                $("#paymentMsg").html('');
                            }, 5000)
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                        console.log("error:" + errorThrown);
                    }
                });
            }
        });
    }
    document.getElementById(divid).style.display = "block";
}

function getuserdetails() {
    $("#loading").show();
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;

                $.ajax({
                    type: "POST",
                    url: ServerUrl + "getUser",
                    data: JSON.stringify({
                        "getUser": {
                            "userId": UserID,
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        $("#useremail").html(data.User.email);
                        $("#plantype").html(data.User.subscription_type);
                        $("#planperiod").html(data.User.subtext);
                        $("#cancel_membership").html(data.User.canceltext);
                        $("#customeID").val(data.User.customer_id);
                        $("#UID").val(data.User._id);
                        $("#subscription_end").val(data.User.subscription_end);
                        if (data.User.cancel_subscription == true || data.User.subscription_type == 'free') {
                            $("#cancelMember").hide();

                        } else {
                            $("#cancelMember").show();
                        }

                        $("#loading").hide();

                        var i;
                        var plansdata = plans.data;

                        $("#basicprice").html('$' + plansdata[0].price + '/month');
                        $("#premiumprice").html('$' + plansdata[1].price + '/year');

                        if (data.User.subscription_type == plansdata[0].planId) {
                            $(".basicplan").css('display', 'none');
                            $(".pm").addClass("colorred");
                            $(".bc").removeClass("colorred");
                            $("#Yearly").attr('checked', 'checked');
                            $("#price").val(plansdata[1].price);
                            $("#planId").val(plansdata[1].planId);

                        } else if (data.User.subscription_type == plansdata[1].planId) {
                            $(".premiumplan").css('display', 'none');
                            $(".bc").addClass("colorred");
                            $(".pm").removeClass("colorred");
                            $("#Monthly").attr('checked', 'checked');
                            $("#price").val(plansdata[0].price);
                            $("#planId").val(plansdata[0].planId);
                        } else {
                            $(".bc").addClass("colorred");
                            $(".pm").removeClass("colorred");
                            $("#Monthly").attr('checked', 'checked');
                            $("#price").val(plansdata[0].price);
                            $("#planId").val(plansdata[0].planId);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
                        showAlert('Get User error' + errorThrown);

                    },
                    complete: function() {}
                });

            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });

}

function selectPlan(id) {
    var plansdata = plans.data;
    if (id == 'm') {
        $(".bc").addClass("colorred");
        $(".pm").removeClass("colorred");
        $("#Monthly").attr('checked', 'checked');
        $("#price").val(plansdata[0].price);
        $("#planId").val(plansdata[0].planId);
    } else {
        $(".bc").removeClass("colorred");
        $(".pm").addClass("colorred");
        $("#Yearly").attr('checked', 'checked');
        $("#price").val(plansdata[1].price);
        $("#planId").val(plansdata[1].planId);
    }
}

function cancelsubscription() {
    $("#loading").show();
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;

                $.ajax({
                    type: "POST",
                    url: ServerUrl + "cancelSubscription",
                    data: JSON.stringify({
                        "cancelSubscription": {
                            "user_id": UserID,
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        $("#successcancel").html("Update Success");
                        setTimeout(function() {
                                $("#successcancel").html('');
                                loadLocalPage('myAccount.html');
                            }, 5000)
                            // console.log(JSON.stringify(data));
                        $("#loading").hide();

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
                        showAlert('Get User error' + errorThrown);
                        setTimeout(function() {
                            loadLocalPage('myAccount.html');
                        }, 5000)
                    },
                    complete: function() {}
                });

            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });
}


function plusbuttonclick() {
    $("#statusupdate").html("");
    $("#adddevice").toggle();
    if ($("#adddevice").is(':visible')) {
        $("#plusbutton").html('<i class="fa fa-minus iconsize"></i>');
    } else {
        $("#plusbutton").html('<i class="fa fa-plus iconsize"></i>');
    }
}



function addbuttonclick() {
    $("#loading").show();
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;
                var devicecode = $("#devicecode").val();

                $.ajax({
                    type: "POST",
                    url: ServerUrl + "addDevice",
                    data: JSON.stringify({
                        "addDevice": {
                            "code": devicecode,
                            "account": {
                                "_id": UserID
                            }
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        $("#adddevice").toggle();
                        $("#statusupdate").html(data.msg);
                        $("#devicecode").val("");
                        $("#loading").hide();
                        getdevicepaired();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
                        showAlert('addDevice error' + errorThrown);
                    },
                    complete: function() {


                    }
                });

            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });
}

function getdevicepaired() {
    setTimeout(function() {
        $("#statusupdate").html("");
    }, 3000);
    setTimeout(function() {
        $("#statusremoveupdate").html("");
    }, 3000);
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;
                console.log(UserID);
                $.ajax({
                    type: "POST",
                    url: ServerUrl + "fetchPairedDevice",
                    data: JSON.stringify({
                        "fetchPairedDevice": {
                            "user_id": UserID,
                            "id": UserID,
                            "userType": 'web'
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {

                        if (data.pairedDevice.length > 0) {

                            $("#devicedetails").empty();
                            var datahtml = '<table width="100%" border="0" cellspacing="4" cellpadding="4"><tr><th width="90%" class="textredcolor">Device Id </th><th width="10%"></th></tr>';

                            for (i = 0; i < data.pairedDevice.length; i++) {
                                var iddevice = data.pairedDevice[i].device_id;
                                var _iddev = data.pairedDevice[i]._id;
                                var start_timedev = data.pairedDevice[i].start_time;

                                datahtml += '<tr><td>' + iddevice + '<br/><span class="colordarkgrey">' + start_timedev + '</span></td><td><i class="fa fa-times buttoncancelround" onclick=removepairedevice("' + _iddev + '")></i></td></tr>';
                                if (data.pairedDevice.length == i + 1) {
                                    datahtml += '</table>';

                                    $("#devicedetails").html(datahtml);
                                }
                            }
                        } else {
                            $("#devicedetails").html('No Paired Devices');

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#devicedetails").empty();
                        showAlert('Get User error' + errorThrown);
                    },
                    complete: function() {


                    }
                });

            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });
}

function removepairedevice(deviceid) {
    $("#loading").show();
    var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
            var len = results.rows.length,
                i = 0;
            if (len > 0) {
                var UserID = results.rows.item(i).key3;


                $.ajax({
                    type: "POST",
                    url: ServerUrl + "removePairedDevice",
                    data: JSON.stringify({
                        "removePairedDevice": {
                            "_id": deviceid,
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        $("#statusremoveupdate").html("Device Removed Successfully");
                        $("#loading").hide();
                        getdevicepaired();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
                        showAlert('Get User error' + errorThrown);
                    },
                    complete: function() {


                    }
                });

            } else {
                showAlert('No user found.');
                Logout();
            }
        }, null);
    });
}

document.addEventListener("resume", onResume, false);

function onResume() {
    getCarousels();
    recentwatched();
    getMyList();
}
function voiceSearch()
{
	//SpeechRecognizer.startRecognize(success, error, maxMatches, promptString, language);
				var maxMatches = 1;
                var promptString = "Search for Movies, TV Shows, Music & More"; // optional
                var language = "en-US";                     // optional
                window.plugins.speechrecognizer.startRecognize(function(result){
                     $("#search").val(result);
                     SearchResult();
                }, function(errorMessage){
                    console.log("Error message: " + errorMessage);
                }, maxMatches, promptString, language);
}

function myTrim(x) {
     return x.replace(/ /g,'');
}

 
